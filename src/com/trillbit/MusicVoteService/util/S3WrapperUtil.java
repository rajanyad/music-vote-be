package com.trillbit.MusicVoteService.util;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class S3WrapperUtil {

	@Autowired
	private AmazonS3Client amazonS3Client;

//	private PutObjectResult upload(String filePath, String uploadKey) throws FileNotFoundException {
//		return upload(new FileInputStream(filePath), uploadKey);
//	}
	
	private String upload(final String bucket, final InputStream inputStream, final String uploadKey, final ObjectMetadata metaData) {
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, uploadKey, inputStream, metaData);

		putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);

		System.out.println(putObjectRequest.getBucketName());
		amazonS3Client.putObject(putObjectRequest);

		IOUtils.closeQuietly(inputStream);

		S3Object s3Object = amazonS3Client.getObject(new GetObjectRequest(
				bucket, uploadKey));
		String s3URI = s3Object.getObjectContent().getHttpRequest().getURI().toString();
		
		return s3URI;
	}

	public List<String> upload(MultipartFile[] multipartFiles, String bucket) {
		List<String> s3URIResults = new ArrayList<>();

		Arrays.stream(multipartFiles)
				.filter(multipartFile -> !StringUtils.isEmpty(multipartFile.getOriginalFilename()))
				.forEach(multipartFile -> {
					try {
						ObjectMetadata metaData = new ObjectMetadata();
						metaData.setContentType(multipartFile.getContentType());
						metaData.setContentLength(multipartFile.getSize());
						
						//Timestamp timeStamp = new Timestamp(new Date().getTime());
						String uploadKey = multipartFile.getOriginalFilename();
						
						s3URIResults.add(upload(bucket, multipartFile.getInputStream(), uploadKey, metaData));
					} catch (IOException e) {
						e.printStackTrace();
					}
				});

		return s3URIResults;
	}

	public ResponseEntity<byte[]> download(String key, String bucket) throws IOException {
		GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);

		S3Object s3Object = amazonS3Client.getObject(getObjectRequest);

		S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

		byte[] bytes = IOUtils.toByteArray(objectInputStream);

		String fileName = URLEncoder.encode(key, "UTF-8").replaceAll("\\+", "%20");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		httpHeaders.setContentLength(bytes.length);
		httpHeaders.setContentDispositionFormData("attachment", fileName);

		return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);
	}
	
	
	public void update(String bucket, String key) throws IOException {
		GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);

		S3Object s3Object = amazonS3Client.getObject(getObjectRequest);

		S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

		ObjectMetadata metaData = new ObjectMetadata();
		
		upload(bucket, objectInputStream, "done.mp3", metaData);
	}

	public List<S3ObjectSummary> list(String bucket) {
		ObjectListing objectListing = amazonS3Client.listObjects(new ListObjectsRequest().withBucketName(bucket));

		List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();

		return s3ObjectSummaries;
	}
}