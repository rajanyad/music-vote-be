/**
 * @author Rajanya
 *
 */
package com.trillbit.MusicVoteService.controller;

import com.google.common.collect.Lists;
import com.trillbit.MusicVoteService.exception.DuplicateRecordException;
import com.trillbit.MusicVoteService.model.Song;
import com.trillbit.MusicVoteService.model.SongList;
import com.trillbit.MusicVoteService.repository.SongRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import javax.inject.Inject;

@CrossOrigin
@RestController
@ConditionalOnExpression("${controller.portal.enabled:true}")
@RequestMapping(value = "/musicvote", produces = MediaType.APPLICATION_JSON_VALUE)
public class SongController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
    @Inject
    SongRepository songRepository;
    
    @Inject
    S3Controller s3Controller;
    
    @RequestMapping(value = "/song/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Song createSong( @RequestBody Song song ) {
    	if(song.getId() == null && isSongExist(song)) {
    		log.error("Duplicate Song!");
    		throw new DuplicateRecordException(song.getName());
    	}
    	
    	return songRepository.save(song);
    }
    
    @RequestMapping(value = "/song/get/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SongList getAllSongs() {
    	SongList songList = new SongList();
    	songList.setSongs(Lists.newArrayList(songRepository.findAll()));
    	return songList;
    }
    
    @RequestMapping(value = "/song/get/topvote", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Song getTopVotedSong() {
    	Song topSong = null;
    	List<Song> songs = songRepository.findByHasPlayedOrderByVotesDesc(false);
    	if(songs.size() > 0){
    		topSong = songs.get(0);
    		String url = topSong.getUrl();
    		String[] parts = url.split("/");
    		String key = parts[parts.length-1];
    		s3Controller.updateSong(key);
    		songRepository.updateHasPlayed(topSong.getId(), true);
    	}
    	return topSong;
    }
    
    
    @RequestMapping(value = "/song/get/topvotefile", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public String getTopVotedSongFile() {
    	String key = null;
    	List<Song> songs = songRepository.findByHasPlayedOrderByVotesDesc(false);
    	if(songs.size() > 0){
    		Song topSong = songs.get(0);
    		String url = topSong.getUrl();
    		String[] parts = url.split("/");
    		key = parts[parts.length-1];
    		songRepository.updateHasPlayed(topSong.getId(), true);
    	}
    	return key;
    }
    
    @RequestMapping(value = "/song/delete/{songId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteSong( @PathVariable Integer songId ) {
    	songRepository.delete(songId);
    }
    
    @RequestMapping(value = "/song/update/{songId}/votes/{votes}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateSongVote( @PathVariable Integer songId, @PathVariable Integer votes ) {
    	songRepository.updateVotes(songId, votes);
    }
    
    @RequestMapping(value = "/song/update/{songId}/hasPlayed/{hasPlayed}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateSongHasPlayed( @PathVariable Integer songId, @PathVariable boolean hasPlayed ) {
    	songRepository.updateHasPlayed(songId, hasPlayed);
    }
    
    private boolean isSongExist(final Song song) {
    	Song savedSong = songRepository.findByNameIgnoreCase(song.getName());
    	return ((savedSong != null)? true: false );
    }
}