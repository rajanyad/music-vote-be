package com.trillbit.MusicVoteService.controller;

import com.trillbit.MusicVoteService.util.S3WrapperUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
@ConditionalOnExpression("${controller.portal.enabled:true}")
@RequestMapping(value = "/musicvote/S3", produces = MediaType.APPLICATION_JSON_VALUE)
public class S3Controller {
	
	@Autowired
	private S3WrapperUtil s3Wrapper;
	
	@Value("${cloud.aws.s3.bucket.tune}")
	private String tuneBucket;
	
	@Value("${cloud.aws.s3.bucket.image}")
	private String imageBucket;
	
	@RequestMapping(value = "/upload/song", method = RequestMethod.POST)
	public String uploadSong(@RequestParam("file") MultipartFile[] multipartFiles) {
		List<String> s3URIResults= s3Wrapper.upload(multipartFiles, tuneBucket);
		return s3URIResults.get(0);
	}
	
	@RequestMapping(value = "/upload/image", method = RequestMethod.POST)
	public String uploadImage(@RequestParam("file") MultipartFile[] multipartFiles) {
		List<String> s3URIResults= s3Wrapper.upload(multipartFiles,imageBucket);
		return s3URIResults.get(0);
	}
	
	public void updateSong(String key) {
		try {
			s3Wrapper.update(tuneBucket, key);
		} catch (IOException e) {
			//Log
		}
	}
//	@RequestMapping(value = "/download", method = RequestMethod.GET)
//	public ResponseEntity<byte[]> download(@RequestParam String key) throws IOException {
//		return s3Wrapper.download(key);
//	}
}
