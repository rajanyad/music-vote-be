package com.trillbit.MusicVoteService.model;

import java.util.List;

import lombok.Data;

@Data
public class SongList {
    private List<Song> songs;
}
