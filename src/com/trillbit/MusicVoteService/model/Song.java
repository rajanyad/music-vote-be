package com.trillbit.MusicVoteService.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter
@NoArgsConstructor
public class Song implements Serializable{
	/**
	 * Unique ID for Serialized object
	 */
    private static final long serialVersionUID = 8058391821299774436L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
	
    @Column(name = "name", nullable = false)
    private String name;
	
    @Column(name = "votes", nullable = false)
    private Integer votes;
    
    @Column(name = "url", nullable = false)
    private String url;
    
    @Column(name = "releaseDate", nullable = true)
    private Date releaseDate;
	
    @Column(name = "artworkUrl", nullable = true)
    private String artworkUrl;
	
    @Column(name = "collection", nullable = true)
    private String collection;
    
    @Column(name = "genre", nullable = true)
    private String genre;

    @Column(name = "language", nullable = true)
    private String language;
    
    @Column(name = "hasPlayed", nullable = false)
    private boolean hasPlayed;
    
    @Column(name = "duration", nullable = false)
    private Integer duration;
}