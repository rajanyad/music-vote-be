package com.trillbit.MusicVoteService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FOUND, reason = "Duplicate record")
public class DuplicateRecordException extends RuntimeException {

	/**
	 * Unique ID for Serialized object
	 */
	private static final long serialVersionUID = 4657491283614755649L;

	public DuplicateRecordException(String record) {
		super(record + " already exists.");
	}

}