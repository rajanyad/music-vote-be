package com.trillbit.MusicVoteService.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.trillbit.MusicVoteService.repository")
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.trillbit.MusicVoteService.model"})
public class RepositoryConfig {
}
