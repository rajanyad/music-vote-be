/**
 * The main config responsible for bean creation.
 */
/**
 * @author drajanya
 *
 */
package com.trillbit.MusicVoteService.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class AppConfig {
	
}