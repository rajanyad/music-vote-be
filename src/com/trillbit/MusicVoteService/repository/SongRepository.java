/**
 * Repository for questions.
 */
/**
 * @author Rajanya
 */
package com.trillbit.MusicVoteService.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.trillbit.MusicVoteService.model.Song;

@Repository
public interface SongRepository extends CrudRepository<Song,Integer>{
    Song findByNameIgnoreCase(String name);
    
    @Modifying
    @Query("update Song s set s.votes= :votes where s.id=:id")
    @Transactional
    void  updateVotes(@Param("id")Integer id, @Param("votes")Integer votes);
    
    @Modifying
    @Query("update Song s set s.hasPlayed= :hasPlayed where s.id=:id")
    @Transactional
    void  updateHasPlayed(@Param("id")Integer id, @Param("hasPlayed")boolean hasPlayed);
    
    List<Song> findByHasPlayedOrderByVotesDesc(boolean hasPlayed);
}